#!/bin/bash
# 
# Creates simple graphs from the RRD's found in the data directory
# Without arguments, graphs are created from the point where data points in
# the RRD start. An explicit time range can be given as first argument (in
# days).
binname=shum

if [ ! -f ${binname}.cfg ]
then
	echo "Config file ${binname}.cfg does not exist!" >&2
	exit 1
fi

. ${binname}.cfg && test -z "$datadir" && ( echo "Config key 'datadir' missing!" >&2 ; exit 1 )

if [ ! -d $datadir ]
then
	echo "Data dir '$datadir' does not exist!"
	exit 1
fi

for F in ${datadir}/*.rrd
do
	end=$( rrdinfo $F | grep "^last_update" | cut -d'=' -f2 )
	if [ -n "$1" ]
	then
		start=end-${1}d
	else
		step_width=$( rrdinfo $F | grep "^step" | cut -d'=' -f2 )
		step_count=$( rrdinfo $F | grep "^rra\[0\]\.cur_row" | cut -d'=' -f2 )
		start=$(( end - step_width * step_count ))
	fi
	rrdtool graph ${F%*.rrd}_rtt.pdf --vertical-label 'ms' -a PDF --end $end --start $start DEF:roundtrip=${F}:rtt:MAX LINE1:roundtrip#0000FF:RTT 
	rrdtool graph ${F%*.rrd}_loss.pdf --vertical-label '%' -a PDF --end $end --start $start DEF:packetloss=${F}:loss:MAX LINE1:packetloss#FF0000:"% loss"
done
