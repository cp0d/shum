# shum
**s**imple **h**ost-**u**p **m**onitoring is a very simple command-line based
host availability monitoring suite based on the Linux ping command.
_shum_ monitors ICMP packet loss and round trip time and stores time series in
round-robin databases, one per host.
It pings hosts, that's all it does.
_shum_ has been created as a quick means to set up simple long-term 
availability tests or validation of other monitoring mechanisms.

## Requirements
To run _shum_, the _ping_ command and _rrdtool_ are required.

## Installation
_shum_ does not require an explicit installation process, it can simply be
copied to disk.

## Using
Monitoring parameters can be configured in the file shum.cfg, hosts.txt lists
the hosts to be monitored.
Go to the _shum_ root directory and run _shum start_ to start monitoring.
_shum stop_ stops monitoring, further commands are explained by _shum help_.

## Results
Monitoring data is written to round-robin databases within the data directory
(see shum.cfg), one database per host.
See _man rrdtool_ for options to use this data.
The _shum_ package includes _graph.sh_, a script to crate simple graphs from
the monitoring data in PDF format.
Run _graph.sh_ from the _shum_ root directory to create two graphs from each
round-robin database file for round-trip time and packet loss, respectively.
